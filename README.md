# EU Cyber Resilience Act (CRA) Discussion Topics

## What is the CRA?

The [EU Cyber Resilience Act (CRA)](https://digital-strategy.ec.europa.eu/en/policies/cyber-resilience-act) is an upcoming EU Regulation that aims to safeguard consumers and businesses who use software or products with a digital components. It creates mandatory cybersecurity requirements for manufacturers and retailers that extend throughout the product lifecycle and helps consumers and business identify such products through the CE mark.

## CRA Resources

* [Final text of the CRA](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0130_EN.html) ([pdf](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0130_EN.pdf)|[docx](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0130_EN.docx))
* [European Commission Draft standardisation request](https://ec.europa.eu/docsroom/documents/58974)
* [CRA Requirements Standards Mapping](https://www.enisa.europa.eu/publications/cyber-resilience-act-requirements-standards-mapping) - Joint Research Centre & ENISA analysis of existing standards.

## Proposed discussion topics

_Proposed discussion topics need the support of 5 people on the mailing list to get started. Any WG member can propose a discussion topic._

* [Open source stewards](./proposed-topics/open-source-stewards.md)

## WG Resources

- [Antitrust Policy](https://www.eclipse.org/org/documents/Eclipse_Antitrust_Policy.pdf)
- [Code of Conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php)

