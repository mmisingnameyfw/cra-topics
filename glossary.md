[[_TOC_]]

# Glossary

### Open Source Steward

An _Open Source Steward_ is a term defined by the CRA. It describes organizations which are supporting open source software but aren't commercial actors, i.e. open source foundations. From the CRA (Art 3, paragraph 18a):

> open-source software steward means a legal person, other than a manufacturer, that has the purpose or objective of systematically providing support on a sustained basis for the development of specific products with digital elements, qualifying as free and open-source software and intended for commercial activities, and that ensures the viability of those products;
	
